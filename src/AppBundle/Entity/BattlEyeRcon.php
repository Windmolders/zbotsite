<?php

namespace AppBundle\Entity;
use AppBundle\Repository\ActionRepository;

/**
 * BattlEyeRcon
 */
class BattlEyeRcon
{
    /**
     * Action constructor.
     */
    function __construct()
    {
        $this->sharedActions = array(new Action());;
    }


    /**
     * @var int
     */
    private $id;

    /**
     * @var bool
     */
    private $enabled = false;

    /**
     * @var bool
     */
    private $colors = true;

    /**
     * @var array<BeRconServer>
     */
    private $servers = array();

    /**
     * @var array<Action>
     */
    private $sharedActions = array();

    /**
     * @var array<String,String>
     */
    private $permissions = array(
        'players' =>  'rcon-admin',
        'admins' =>  'rcon-admin',
        'bans' =>  'rcon-admin',
        'loadScripts' =>  'rcon-admin',
        'loadEvents' =>  'rcon-admin',
        'say' =>  'rcon-admin',
        'missions' =>  'rcon-admin',
        'version' =>  'rcon-admin',
        'update' =>  'rcon-admin',
        'loadBans' =>  'rcon-admin',
        'writeBans' =>  'rcon-admin',
        'removeBan' =>  'rcon-admin',
        'ban' =>  'rcon-admin',
        'addBan' =>  'rcon-admin',
        'MaxPing' =>  'rcon-admin',
        'kick' =>  'rcon-admin',
        'serverCommands' =>  'rcon-admin',
        'disconnect' =>  'rcon-admin',
        'exit' =>  'rcon-admin',
    );


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return BattlEyeRcon
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return bool
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set colors
     *
     * @param boolean $colors
     *
     * @return BattlEyeRcon
     */
    public function setColors($colors)
    {
        $this->colors = $colors;

        return $this;
    }

    /**
     * Get colors
     *
     * @return bool
     */
    public function getColors()
    {
        return $this->colors;
    }

    /**
     * Set servers
     *
     * @param array $servers
     *
     * @return BattlEyeRcon
     */
    public function setServers($servers)
    {
        $this->servers = $servers;

        return $this;
    }

    /**
     * Get servers
     *
     * @return array
     */
    public function getServers()
    {
        return $this->servers;
    }

    /**
     * Set sharedActions
     *
     * @param array $sharedActions
     *
     * @return BattlEyeRcon
     */
    public function setSharedActions($sharedActions)
    {
        $this->sharedActions = $sharedActions;

        return $this;
    }

    /**
     * Get sharedActions
     *
     * @return array<Action>
     */
    public function getSharedActions()
    {
        return $this->sharedActions;
    }

    /**
     * Set permissions
     *
     * @param array $permissions
     *
     * @return BattlEyeRcon
     */
    public function setPermissions($permissions)
    {
        $this->permissions = $permissions;

        return $this;
    }

    /**
     * Get permissions
     *
     * @return array<String,String>
     */
    public function getPermissions()
    {
        return $this->permissions;
    }
}


<?php

namespace AppBundle\Entity;

/**
 * Job
 */
class Job
{
    /**
     * Static constructor / factory
     * return Job
     */
    public static function create($time, $text) {
        $instance = new self();
        $instance->time = $time;
        $instance->text = $text;
        return $instance;
    }

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $time;

    /**
     * @var string
     */
    private $text;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set time
     *
     * @param string $time
     *
     * @return Job
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return string
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Job
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }
}


<?php

namespace AppBundle\Entity;

/**
 * BeRconServer
 */
class BeRconServer
{
    /**
     * Action constructor.
     */
    function __construct()
    {
        $this->actions = array(new Action());;
        $this->jobs = array(new Job());;
    }

    /**
     * @var int
     */
    private $id;

    /**
     * @var float
     */
    private $timezone = 2;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $ip;

    /**
     * @var string
     */
    private $port;

    /**
     * @var string
     */
    private $rconPassword;

    /**
     * @var array<Job>
     */
    private $jobs;

    /**
     * @var array<Action>
     */
    private $actions;

    /**
     * @var BattlEyeRcon
     */
    private $bercon;

    /**
     * @var bool
     */
    private $deleted = false;

    /**
     * @var array<string,string> $channels
     */
    private $channels = array (
        'side' => 'rcon',
        'direct' => 'rcon',
        'vehicle' => 'rcon',
        'group' => 'rcon',
        'admin' => 'rcon',
        'default' => 'rcon',
        'commands' => 'rcon',
        'joins' => 'rcon',
        'global' => 'rcon',
    );
    /**
     * @var array<string,bool> $channels
     */
    private $showChannels = array (
        'side' => true,
        'direct' => true,
        'vehicle' => true,
        'group' => true,
        'admin' => true,
        'default' => true,
        'commands' => true,
        'joins' => true,
        'global' => true,
    );

    /**
     * @return BattlEyeRcon
     */
    public function getBercon()
    {
        return $this->bercon;
    }

    /**
     * @param BattlEyeRcon $bercon
     */
    public function setBercon($bercon)
    {
        $this->bercon = $bercon;
    }


    /**
     * @return array
     */
    public function getActions()
    {
        return $this->actions;
    }

    /**
     * @param array<Action> $actions
     */
    public function setActions($actions)
    {
        $this->actions = $actions;
    }

    /**
     * @return array
     */
    public function getJobs()
    {
        return $this->jobs;
    }

    /**
     * @param array $jobs
     */
    public function setJobs($jobs)
    {
        $this->jobs = $jobs;
    }



    /**
     * @return array
     */
    public function getShowChannels()
    {
        return $this->showChannels;
    }

    /**
     * @param array $showChannels
     */
    public function setShowChannels($showChannels)
    {
        $this->showChannels = $showChannels;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set timezone
     *
     * @param float $timezone
     *
     * @return BeRconServer
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;

        return $this;
    }

    /**
     * Get timezone
     *
     * @return float
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return BeRconServer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return BeRconServer
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set port
     *
     * @param string $port
     *
     * @return BeRconServer
     */
    public function setPort($port)
    {
        $this->port = $port;

        return $this;
    }

    /**
     * Get port
     *
     * @return string
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * Set rconPassword
     *
     * @param string $rconPassword
     *
     * @return BeRconServer
     */
    public function setRconPassword($rconPassword)
    {
        $this->rconPassword = $rconPassword;

        return $this;
    }

    /**
     * Get rconPassword
     *
     * @return string
     */
    public function getRconPassword()
    {
        return $this->rconPassword;
    }

    /**
     * Set channels
     *
     * @param array $channels
     *
     * @return BeRconServer
     */
    public function setChannels($channels)
    {
        $this->channels = $channels;

        return $this;
    }

    /**
     * Get channels
     *
     * @return array
     */
    public function getChannels()
    {
        return $this->channels;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }


}


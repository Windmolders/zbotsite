<?php

namespace AppBundle\Entity;

/**
 * Action
 */
class Action
{
    /**
     * Static constructor / factory
     */
    public static function create($command, $reply, $discordReply, $role) {
        $instance = new self();
        $instance->command = $command;
        $instance->reply = $reply;
        $instance->discordReply = $discordReply;
        $instance->role = $role;
        return $instance;
    }

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $command;

    /**
     * @var string
     */
    private $reply;

    /**
     * @var string
     */
    private $discordReply;

    /**
     * @var string
     */
    private $role;

    /**
     * @var BattlEyeRcon
     */
    private $bercon;

    /**
     * @return BattlEyeRcon
     */
    public function getBercon()
    {
        return $this->bercon;
    }

    /**
     * @param BattlEyeRcon $bercon
     */
    public function setBercon($bercon)
    {
        $this->bercon = $bercon;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set command
     *
     * @param string $command
     *
     * @return Action
     */
    public function setCommand($command)
    {
        $this->command = $command;

        return $this;
    }

    /**
     * Get command
     *
     * @return string
     */
    public function getCommand()
    {
        return $this->command;
    }

    /**
     * Set reply
     *
     * @param string $reply
     *
     * @return Action
     */
    public function setReply($reply)
    {
        $this->reply = $reply;

        return $this;
    }

    /**
     * Get reply
     *
     * @return string
     */
    public function getReply()
    {
        return $this->reply;
    }

    /**
     * Set discordReply
     *
     * @param string $discordReply
     *
     * @return Action
     */
    public function setDiscordReply($discordReply)
    {
        $this->discordReply = $discordReply;

        return $this;
    }

    /**
     * Get discordReply
     *
     * @return string
     */
    public function getDiscordReply()
    {
        return $this->discordReply;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return Action
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }
}


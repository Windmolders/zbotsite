<?php

namespace AppBundle\Entity;

/**
 * DiscordServer
 */
class DiscordServer
{
    function __construct()
    {
        $this->bercon = new BattlEyeRcon();
    }

    /**
     * @var int
     */
    private $id;

    /**
     * @var BattlEyeRcon
     */
    private $bercon;

    /**
     * @var string
     */
    private $guildId;

    /**
     * @var array
     */
    private $channels;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bercon
     *
     * @param string $bercon
     *
     * @return DiscordServer
     */
    public function setBercon($bercon)
    {
        $this->bercon = $bercon;

        return $this;
    }

    /**
     * Get bercon
     *
     * @return BattlEyeRcon
     */
    public function getBercon()
    {
        return $this->bercon;
    }

    /**
     * Set guildId
     *
     * @param string $guildId
     *
     * @return DiscordServer
     */
    public function setGuildId($guildId)
    {
        $this->guildId = $guildId;

        return $this;
    }

    /**
     * Get guildId
     *
     * @return string
     */
    public function getGuildId()
    {
        return $this->guildId;
    }

    /**
     * Set channels
     *
     * @param array $channels
     *
     * @return DiscordServer
     */
    public function setChannels($channels)
    {
        $this->channels = $channels;

        return $this;
    }

    /**
     * Get channels
     *
     * @return array
     */
    public function getChannels()
    {
        return $this->channels;
    }
}


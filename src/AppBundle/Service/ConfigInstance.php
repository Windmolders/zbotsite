<?php

namespace AppBundle\Service;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\SiteSetting;
use AppBundle\Repository\SiteSettingRepository;

class ConfigInstance extends Controller
{
    /** @var  array|null */
    private $configs;

    /** @var  SiteSettingRepository */
    private $siteSettingRepository;

    /**
     * @return array|null
     */
    protected function getConfigs()
    {
        return $this->configs;
    }

    /**
     * @param array|null $configs
     */
    public function setConfigs($configs)
    {
        $this->configs = $configs;
    }

    /**
     * @return SiteSettingRepository
     */
    protected function getSiteSettingRepository()
    {
        return $this->siteSettingRepository;
    }

    /**
     * @param SiteSettingRepository $siteSettingRepository
     */
    public function setSiteSettingRepository($siteSettingRepository)
    {
        $this->siteSettingRepository = $siteSettingRepository;
    }

    public function __construct(SiteSettingRepository $siteSettingRepository)
    {
       $this->siteSettingRepository = $siteSettingRepository;
    }

    /**
     *
     */
    public function getConfigValue($string, $default = '')
    {
        if(!is_null($this->configs)) {
            if(isset($this->configs[$string])) {
                return $this->configs[$string]->getValue();
            }
            return $default;
        }

        $this->getAllConfigs();

        return $this->getConfigValue($string, $default);
    }

    public function getConfigObject($string, $default = null)
    {
        if(!is_null($this->configs)) {
            if(isset($this->configs[$string])) {
                return $this->configs[$string];
            }
            return $default;
        }

        $this->getAllConfigs();

        return $this->getConfigObject($string);
    }

    protected function getAllConfigs()
    {
        $allConfigs = $this->getSiteSettingRepository()->findAll();
        $allConfigsArray = array();

        foreach($allConfigs as $config) {
            /** @var SiteSetting $config */
            $allConfigsArray[$config->getName()] = $config;
        }

        $this->configs = $allConfigsArray;
    }

   
}
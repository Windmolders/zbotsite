<?php

namespace AppBundle\Controller;

use AppBundle\Entity\SiteSetting;
use Clastic\CoreBundle\Module\ModuleInterface;
use Clastic\CoreBundle\Module\SubmoduleInterface;
use Clastic\NodeBundle\Module\NodeModuleInterface;
use Doctrine\ORM\QueryBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

class HelperController extends Controller
{
    public function getSetting($name, $default = '') {
        $sitesettings = $this->getDoctrine()->getRepository('AppBundle:SiteSetting');

        /** @var SiteSetting $value */
        $setting = $sitesettings->findOneBy(array('name' => $name));

        if (is_null($setting)) {
            return $default;
        }

        return $setting->getValue();
    }

    /**
     * @param QueryBuilder $queryBuilder
     *
     * @return QueryBuilder
     */
    protected function alterListQuery(QueryBuilder $queryBuilder)
    {
        return $queryBuilder;
    }

    /**
     * @param ModuleInterface $module
     *
     * @return SubmoduleInterface[]
     */
    private function getSubmodules(ModuleInterface $module)
    {
        if ($module instanceof SubmoduleInterface) {
            $module = $this->getModule($module->getParentIdentifier());
        }

        return $this->get('clastic.module_manager')
            ->getSubmodules($module->getIdentifier());
    }

    /**
     * @param string $identifier
     *
     * @return ModuleInterface
     */
    private function getModule($identifier)
    {
        return $this->get('clastic.module_manager')
            ->getModule($identifier);
    }

    /**
     * @param string $type
     * @param string $root
     *
     * @return Breadcrumbs
     */
    protected function buildBreadcrumbs($type, $root)
    {
        /** @var Breadcrumbs $breadcrumbs */
        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('navigation.home', $this->get('router')->generate('clastic_backoffice_dashboard'));

        /** @var NodeModuleInterface $module */
        $module = $this->get('clastic.module_manager')->getModule($type);
        if ($module) {
            $breadcrumbs->addItem($module->getName(), $this->get('router')->generate($root, array(
                'type' => $type,
            )));
        }

        return $breadcrumbs;
    }
}

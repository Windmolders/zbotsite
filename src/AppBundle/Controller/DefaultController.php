<?php

namespace AppBundle\Controller;

use AppBundle\Entity\DiscordServer;
use AppBundle\Object\Action;
use AppBundle\Object\Bercon;
use AppBundle\Object\BerconServer;
use AppBundle\Object\Job;
use AppBundle\Object\Server;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Cache\Adapter\RedisAdapter;

class DefaultController extends Controller
{
    /**
     * @Route("/convert", name="convert")
     */
    public function convertAction(Request $request)
    {
        $redis = $this->container->get('snc_redis.default');

        $redis->select(0);

        $em = $this->getDoctrine()->getEntityManager();

        $keys = $redis->keys('guild:*');

        foreach ($keys as $key) {
            $this->convertGuild($key, $em);
        }

        $em->flush();

        // if you don't know the data to send when creating the response
        $response = new JsonResponse();
// ...
        $response->setData(array('done' => true));


        return $response;
    }

    /**
     * @param string $key
     * @param EntityManager $em
     */
    public function convertGuild($key, $em) {
        $redis = $this->container->get('snc_redis.default');

        //$val = $redis->get('guild:3');
        $val = $redis->get($key);

        $rconserver = new DiscordServer();

        if(!is_null($val)) {
            $objectVal = json_decode($val, false);

            $rconserver->setChannels((array) $objectVal->channels);

            $rconserver->setGuildId($objectVal->guildId);

            $objectBercon = $objectVal->bercon;

            //$server->set((array) $objectVal->channels);

            $bercon = $rconserver->getBercon();

            $bercon->setColors(true);

            $bercon->setEnabled($objectBercon->enabled);
            $bercon->setPermissions((array)$objectBercon->permissions);

            $objectActions = (array) $objectBercon->sharedActions;

            $sharedActions = array();

            foreach ($objectActions as $oAction) {

                $role = '';

                if(property_exists($oAction,'role')) {
                    $role = $oAction->role;
                }

                $action = \AppBundle\Entity\Action::create(
                    $oAction->command,
                    $oAction->reply,
                    $oAction->discordReply,
                    $role
                );
                array_push($sharedActions, $action);
            }

            if(count($sharedActions) > 0) {
                $bercon->setSharedActions($sharedActions);
            }


            $objectServers = (array) $objectBercon->servers;

            $servers = array();

            $index = 0;

            foreach ($objectServers as $oServer) {
                $server = new \AppBundle\Entity\BeRconServer();
                $server->setChannels((array) $oServer->channels);

                $server->setIp($oServer->ip);
                $server->setName($oServer->name);
                $server->setPort($oServer->port);
                $server->setRconPassword($oServer->rconPassword);

                if(property_exists($oAction,'showChannels')) {
                    $server->setShowChannels((array) $oServer->showChannels);
                }

                if(property_exists($oAction,'timezone')) {
                    $server->setTimezone($oServer->timezone);
                }

                $actions = array();

                if(property_exists($oServer,'actions')) {
                    $objectActions = (array) $oServer->actions;

                    foreach ($objectActions as $oAction) {

                        $role = '';

                        if(property_exists($oAction,'role')) {
                            $role = $oAction->role;
                        }

                        $action =  \AppBundle\Entity\Action::create(
                            $oAction->command,
                            $oAction->reply,
                            $oAction->discordReply,
                            $role
                        );
                        array_push($actions, $action);
                    }
                }

                if(count($actions) > 0) {
                    $server->setActions($actions);
                }

                $jobs = array();

                if(property_exists($oServer,'jobs')) {
                    $objectJobs = (array) $oServer->jobs;
                    foreach ($objectJobs as $oJob) {
                        $job = \AppBundle\Entity\Job::create(
                            $oJob->time,
                            $oJob->text
                        );
                        array_push($jobs, $job);
                    }
                }

                if(count($jobs) > 0) {
                    $server->setJobs($jobs);
                }

                $server->setBercon($bercon);

                $em->persist($server);

                array_push($servers, $server);

            }

            $bercon->setServers($servers);

            $em->persist($bercon);

            $rconserver->setBercon($bercon);

            $em->persist($rconserver);
        }


    }




    /**
     * @Route("/old", name="homepage-old")
     */
    public function indexAction(Request $request)
    {
        $provider = new \Discord\OAuth\Discord([
            'clientId' => $this->container->getParameter('discord_client_id'),
            'clientSecret' => $this->container->getParameter('discord_client_secret'),
            'redirectUri' => $this->container->getParameter('redirect_url'),
        ]);

        $scopes = array ( 'scope' => array(
            'identify',
            'guilds')
        );

        return $this->render(
            'AppBundle:Default:index.html.twig',
            array(
                'provider' => $provider,
                'user' => null,
                'guidl' => null,
                'options' => $scopes,
            )
        );
    }

    /**
     * @Route("/old-servers", name="old-discord-auth")
     */
    public function authAction(Request $request)
    {
        /** @var Session $session */
        $session = $this->get("session");

        $token = $session->get('discord-token');

        $provider = new \Discord\OAuth\Discord([
            'clientId' => $this->container->getParameter('discord_client_id'),
            'clientSecret' => $this->container->getParameter('discord_client_secret'),
            'redirectUri' => $this->container->getParameter('redirect_url'),
        ]);

        $guilds = null;
        $user = null;
        $ownedGuilds = [];

        if (!is_null($token)) {

            // Get the user object.
            $user = $provider->getResourceOwner($token);

            // Get the guilds and connections.
            $guilds = $user->guilds;

            foreach($guilds as $guild) {

                $permissions = $guild->__get('permissions');

                if($guild->__get('owner') == $user->getId()) {
                    array_push($ownedGuilds, $guild);
                } elseif (
                (bool) (($permissions >> 5) & 1)
                ) {
                    array_push($ownedGuilds, $guild);
                }
            }

            return $this->render(
                'AppBundle:Default:profile.html.twig',
                array(
                    'provider' => $provider,
                    'guilds' => $ownedGuilds,
                    'user' => $user,
                )
            );

        } elseif( $request->get('code')) {
            $token = $provider->getAccessToken('authorization_code', [
                'code' => $request->get('code'),
            ]);

            // Get the user object.
            $user = $provider->getResourceOwner($token);

            // Get the guilds and connections.
            $guilds = $user->guilds;

            // Store the new token.
            $session->set('discord-token', $token);
            //  $session->set('discord-guilds', $guilds);
            //  $session->set('discord-user', $user);

            foreach($guilds as $guild) {
                if($guild->__get('owner_id') == $user->getId()) {
                    array_push($ownedGuilds, $guild);
                }
            }

            return $this->render(
                'AppBundle:Default:profile.html.twig',
                array(
                    'provider' => $provider,
                    'guilds' => $ownedGuilds,
                    'user' => $user,
                )
            );
        }

        return $this->redirectToRoute('homepage');
    }


    /**
     * @Route("/old/guild/{guildId}", name="old_server_form")
     */
    public function serverAction($guildId, Request $request)
    {
        /** @var Session $session */
        $session = $this->get("session");

        $token = $session->get('discord-token');

        if (!is_null($token)) {

            $provider = new \Discord\OAuth\Discord([
                'clientId' => $this->container->getParameter('discord_client_id'),
                'clientSecret' => $this->container->getParameter('discord_client_secret'),
                'redirectUri' => $this->container->getParameter('redirect_url'),
                'scope' => 'identify guilds'
            ]);

            // Get the user object.
            $user = $provider->getResourceOwner($token);

            // Get the guilds and connections.
            $guilds = $user->guilds;

            $ownsGuild = false;
            $guild = null;

            foreach($guilds as $loopguild) {

                if($loopguild->__get('id') == $guildId) {
                    $ownsGuild = true;
                    $guild = $loopguild;
                }
            }

            if($ownsGuild) {
                $rconserver = $this->getGuild($guildId);
                $data = $this->mapGuildForm($rconserver, $request->request->all());
                return $this->render(
                    'AppBundle:Default:guild.html.twig',
                    array(
                        'provider' => $provider,
                        'guild' => $guild,
                        'user' => $user,
                        'server' => $data['server'],
                        'saved' => $data['saved'],
                    )
                );
            } else {

                return $this->redirectToRoute('homepage');
            }
        }

        return $this->redirectToRoute('homepage');
    }

    /**
     * Maps the form submission on the correct fields ( Must be made into symfony forms one day
     * Temp solution
     * @param Server $rconserver
     * @param array $postData
     * @return array
     */
    public function mapGuildForm($rconserver, $postData)
    {
        $saved = false;
        if(!is_null($postData) && count($postData) > 0 )
        {
            $sharedActions = array();
            $bercon = $rconserver->getBercon();

            $enabledPassed = false;

            foreach ($postData as $key => $value)
            {
                if($key == 'enabled') {
                    $enabledPassed = true;
                    $bercon->setEnabled(true);
                }
                if($key == 'colors') {
                    $bercon->setColors($value);
                }

                if(strpos($key, 'command') !== false) {
                    $index = substr($key, 7);

                    $action = new Action($value, $postData['reply' . $index], $postData['discordReply' . $index], $postData['role' . $index] );
                    array_push($sharedActions, $action);
                }

                if($key == 'permissions') {
                    $bercon->setPermissions($value);
                }
            }

            if(!$enabledPassed) {
                $bercon->setEnabled(false);
            }

            if(count($sharedActions) > 0) {
                $bercon->setSharedActions($sharedActions);
            }

            $this->saveGuild($rconserver);
            $saved = true;
        }

        return array(
            'server' => $rconserver,
            'saved' => $saved,
        );
    }

    /**
     * Maps the bercon form submission on the correct fields ( Must be made into symfony forms one day
     * Temp solution
     * @param Server $rconserver
     * @param BerconServer $berconserver
     * @param array $postData
     * @return array
     */
    public function mapBerconForm($rconserver, $berconserver, $postData, $isNew)
    {
        $saved = false;
        if(!is_null($postData) && count($postData) > 0 )
        {
            $actions = array();
            $jobs = array();

            $bercon = $rconserver->getBercon();

            $newBerconServerId = count($bercon->getServers());

            foreach ($postData as $key => $value)
            {
                if($key == 'name') {
                    $berconserver->setName($value);
                }
                if($key == 'ip') {
                    $berconserver->setIp($value);
                }
                if($key == 'port') {
                    $berconserver->setPort($value);
                }
                if($key == 'rconPassword') {
                    $berconserver->setRconPassword($value);
                }
                if($key == 'channels') {
                    $berconserver->setChannels($value);
                }
                if($key == 'timezone') {
                    $berconserver->setTimezone($value);
                }
                if($key == 'showChannels') {
                    $defaultValues = $berconserver->getShowChannels();
                    $newValues = array();
                    foreach ($defaultValues as $showKey => $showValue) {
                        if(array_key_exists($showKey, $value) && $value[$showKey] == 'on') {
                            $newValues[$showKey] = true;
                        } else {
                            $newValues[$showKey] = false;
                        }
                    }

                    $newValues['default'] = true;
                    $newValues['commands'] = true;

                    $berconserver->setShowChannels($newValues);
                }
                if(strpos($key, 'command') !== false) {
                    $index = substr($key, 7);

                    $action = new Action($value, $postData['reply' . $index], $postData['discordReply' . $index], $postData['role' . $index] );
                    array_push($actions, $action);
                }
                if(strpos($key, 'jobtime') !== false) {
                    $index = substr($key, 7);

                    $job = new Job(
                        $postData['jobtime' . $index],
                        $postData['jobtext' . $index]
                    );

                    array_push($jobs, $job);
                }
            }

            if(count($actions) > 0) {
                $berconserver->setActions($actions);
            }

            if(count($jobs) > 0) {
                $berconserver->setJobs($jobs);
            }

            if($isNew) {
                $berconserver->setId($newBerconServerId);
                $servers = $bercon->getServers();
                array_push($servers, $berconserver);
                $rconserver->getBercon()->setServers($servers);
            }

            $this->saveGuild($rconserver);

            $saved = true;

        } else {
            if($isNew) {
                $berconserver->setId(count($rconserver->getBercon()->getServers()));
            }
        }

        return array(
            'server' => $rconserver,
            'berconServer' => $berconserver,
            'saved' => $saved,
        );
    }


    /**
     * @Route("/old/guild/{guildId}/bercon/{berconId}", name="old_bercon_form")
     */
    public function berconAction($guildId, $berconId, Request $request)
    {
        /** @var Session $session */
        $session = $this->get("session");

        $token = $session->get('discord-token');

        if (!is_null($token)) {

            $provider = new \Discord\OAuth\Discord([
                'clientId' => $this->container->getParameter('discord_client_id'),
                'clientSecret' => $this->container->getParameter('discord_client_secret'),
                'redirectUri' => $this->container->getParameter('redirect_url'),
                'scope' => 'identify guilds'
            ]);

            // Get the user object.
            $user = $provider->getResourceOwner($token);

            // Get the guilds and connections.
            $guilds = $user->guilds;

            $ownsGuild = false;
            $guild = null;

            foreach($guilds as $loopguild) {
                if($loopguild->__get('id') == $guildId) {
                    $ownsGuild = true;
                    $guild = $loopguild;
                }
            }

            if($ownsGuild) {

                $rconserver = $this->getGuild($guildId);

                $berconserver = new BerconServer();

                $isNew = true;


                if($berconId != 'new') {
                    foreach ($rconserver->getBercon()->getServers() as $server) {
                        if ($berconId == $server->getId()) {
                            $berconserver = $server;
                            $isNew = false;
                        }
                    }
                }

                $data = $this->mapBerconForm($rconserver, $berconserver, $request->request->all(), $isNew);

                return $this->render(
                    'AppBundle:Default:bercon.html.twig',
                    array(
                        'provider' => $provider,
                        'guild' => $guild,
                        'user' => $user,
                        'server' => $data['server'],
                        'bercon' => $data['berconServer'],
                        'saved' => $data['saved']
                    )
                );
            } else {
                return $this->redirectToRoute('homepage');
            }
        }

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/old/guild/{guildId}/bercon/{berconId}/delete", name="old-bercon_form_delete")
     */
    public function berconDeleteAction($guildId, $berconId)
    {
        /** @var Session $session */
        $session = $this->get("session");

        $token = $session->get('discord-token');

        if (!is_null($token)) {

            $provider = new \Discord\OAuth\Discord([
                'clientId' => $this->container->getParameter('discord_client_id'),
                'clientSecret' => $this->container->getParameter('discord_client_secret'),
                'redirectUri' => $this->container->getParameter('redirect_url'),
                'scope' => 'identify guilds'
            ]);

            // Get the user object.
            $user = $provider->getResourceOwner($token);

            // Get the guilds and connections.
            $guilds = $user->guilds;

            $ownsGuild = false;
            $guild = null;

            foreach($guilds as $loopguild) {
                if($loopguild->__get('id') == $guildId) {
                    $ownsGuild = true;
                    $guild = $loopguild;
                }
            }

            if($ownsGuild) {


                $rconserver = $this->getGuild($guildId);


                $berconserver = null;

                $servers = $rconserver->getBercon()->getServers();

                foreach ($servers as $key => $server) {
                    if($berconId == $server->getId()) {
                        unset($servers[$key]);
                    }
                }

                $rconserver->getBercon()->setServers($servers);

                $this->saveGuild($rconserver);

                return $this->render(
                    'AppBundle:Default:deleted.html.twig',
                    array(
                        'provider' => $provider,
                        'guild' => $guild,
                        'user' => $user,
                    )
                );

            } else {
                return $this->redirectToRoute('homepage');
            }
        }
        return $this->redirectToRoute('homepage');
    }

    public function getCache() {
        return $this->get('cache.app');
    }


    /**
     * @param string $id
     * @return Server
     */
    public function getGuild($id) {
        $redis = $this->container->get('snc_redis.default');

        //$val = $redis->get('guild:3');
        $val = $redis->get('guild:' . $id);

        $rconserver = new Server();

        $rconserver->setGuildId($id);

        if(is_null($val)) {
            // new new
        } else {
            $objectVal = json_decode($val, false);

            $rconserver->setChannels((array) $objectVal->channels);

            $objectBercon = $objectVal->bercon;

            //$server->set((array) $objectVal->channels);

            $bercon = $rconserver->getBercon();

            $bercon->setColors(true);

            $bercon->setEnabled($objectBercon->enabled);
            $bercon->setPermissions((array)$objectBercon->permissions);

            $objectActions = (array) $objectBercon->sharedActions;

            $sharedActions = array();

            foreach ($objectActions as $oAction) {

                $role = '';

                if(property_exists($oAction,'role')) {
                    $role = $oAction->role;
                }

                $action = new Action(
                    $oAction->command,
                    $oAction->reply,
                    $oAction->discordReply,
                    $role
                );
                array_push($sharedActions, $action);
            }

            if(count($sharedActions) > 0) {
                $bercon->setSharedActions($sharedActions);
            }


            $objectServers = (array) $objectBercon->servers;

            $servers = array();

            $index = 0;

            foreach ($objectServers as $oServer) {
                $server = new BerconServer();
                $server->setId($index);
                $index++;
                $server->setChannels((array) $oServer->channels);
                $server->setShowChannels((array) $oServer->showChannels);
                $server->setIp($oServer->ip);
                $server->setName($oServer->name);
                $server->setPort($oServer->port);
                $server->setRconPassword($oServer->rconPassword);
                $server->setTimezone($oServer->timezone);

                $actions = array();

                if(property_exists($oServer,'actions')) {
                    $objectActions = (array) $oServer->actions;

                    foreach ($objectActions as $oAction) {

                        $role = '';

                        if(property_exists($oAction,'role')) {
                            $role = $oAction->role;
                        }

                        $action = new Action(
                            $oAction->command,
                            $oAction->reply,
                            $oAction->discordReply,
                            $role
                        );
                        array_push($actions, $action);
                    }
                }

                if(count($actions) > 0) {
                    $server->setActions($actions);
                }

                $jobs = array();

                if(property_exists($oServer,'jobs')) {
                    $objectJobs = (array) $oServer->jobs;
                    foreach ($objectJobs as $oJob) {
                        $job = new Job(
                            $oJob->time,
                            $oJob->text
                        );
                        array_push($jobs, $job);
                    }
                }

                if(count($jobs) > 0) {
                    $server->setJobs($jobs);
                }

                array_push($servers, $server);

                $bercon->setServers($servers);

                $rconserver->setBercon($bercon);


            }
        }

        return $rconserver;
    }

    /**
     * @param $guild Server
     */
    public function saveGuild($guild)
    {
        $server = new \stdClass();
        $bercon = new \stdClass();

        $guildBercon = $guild->getBercon();

        $server->guildId = $guild->getGuildId();
        $server->channels = $guild->getChannels();

        $bercon->enabled = $guildBercon->isEnabled();
        $bercon->colors = $guildBercon->isColors();
        $bercon->permissions = $guildBercon->getPermissions();


        $berconservers = array();
        $sharedActions = array();

        foreach ($guildBercon->getSharedActions() as $guildAction) {

            /** @var Action $guildAction */
            $action = new \stdClass();
            $action->command = $guildAction->getCommand();
            $action->reply = $guildAction->getReply();
            $action->discordReply = $guildAction->getDiscordReply();
            $action->role = $guildAction->getRole();
            array_push($sharedActions, $action);
        }

        $bercon->sharedActions = $sharedActions;

        foreach ($guildBercon->getServers() as $guildServer) {
            /** @var BerconServer $guildServer */
            $berconserver = new \stdClass();

            $berconserver->id = $guildServer->getId();
            $berconserver->name = $guildServer->getName();
            $berconserver->ip = $guildServer->getIp();
            $berconserver->port = $guildServer->getPort();
            $berconserver->timezone = $guildServer->getTimezone();
            $berconserver->rconPassword = $guildServer->getRconPassword();
            $berconserver->channels = $guildServer->getChannels();
            $berconserver->showChannels = $guildServer->getShowChannels();

            $sharedActions = array();

            foreach ($guildServer->getActions() as $rconAction) {
                /** @var Action $rconAction */
                $action = new \stdClass();
                $action->command = $rconAction->getCommand();
                $action->reply = $rconAction->getReply();
                $action->discordReply = $rconAction->getDiscordReply();
                $action->role = $rconAction->getRole();
                array_push($sharedActions, $action);
            }

            $berconserver->actions = $sharedActions;


            $jobs = array();

            foreach ($guildServer->getJobs() as $job) {
                /** @var Job $job */
                $jobObject = new \stdClass();
                $jobObject->time = $job->getTime() ;
                $jobObject->text = $job->getText();
                array_push($jobs, $jobObject);
            }

            $berconserver->jobs = $jobs;

            array_push($berconservers, $berconserver);
        }

        $bercon->servers = $berconservers;


        $server->bercon = $bercon;

        $json = json_encode($server);

        $redis = $this->container->get('snc_redis.default');
        //$redis->set('guild:3', $json);
        $redis->set('guild:' . $guild->getGuildId(), $json);
    }
}

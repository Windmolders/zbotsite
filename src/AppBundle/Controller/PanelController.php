<?php

namespace AppBundle\Controller;

use AppBundle\Entity\BattlEyeRcon;
use AppBundle\Entity\BeRconServer;
use AppBundle\Entity\DiscordServer;
use AppBundle\Entity\Action;
use AppBundle\Entity\Job;
use AppBundle\Repository\BeRconServerRepository;
use AppBundle\Repository\DiscordServerRepository;
use Discord\OAuth\Parts\Guild;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Cache\Adapter\RedisAdapter;

class PanelController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function homeAction(Request $request)
    {
        /** @var Session $session */
        $session = $this->get("session");

        $token = $session->get('discord-token');

        $info = $this->getDiscordInfo($token);

        return $this->render(
            'AppBundle:Panel:home.html.twig',
            array(
                'user' => $info['user'],
                'guilds' => $info['guilds'],
                'record' => array(
                    'class' => '',
                    'displayTitle' => 'Home',
                ),
                'activeGuild' => $this->getActiveGuild($session)
            )
        );
    }

    /**
     * @Route("/commands", name="commands")
     */
    public function commandsAction(Request $request)
    {
        /** @var Session $session */
        $session = $this->get("session");

        $token = $session->get('discord-token');

        $info = $this->getDiscordInfo($token);

        return $this->render(
            'AppBundle:Panel:commands.html.twig',
            array(
                'user' => $info['user'],
                'guilds' => $info['guilds'],
                'record' => array(
                    'class' => '',
                    'displayTitle' => 'Home',
                ),
                'activeGuild' => $this->getActiveGuild($session)
            )
        );
    }


    /**
     * @Route("/start", name="homepage")
     */
    public function indexAction(Request $request)
    {
        /** @var Session $session */
        $session = $this->get("session");

        $token = $session->get('discord-token');

        $info = $this->getDiscordInfo($token);

        return $this->render(
            'AppBundle:Panel:index.html.twig',
            array(
                'user' => $info['user'],
                'guilds' => $info['guilds'],
                'record' => array(
                    'class' => '',
                    'displayTitle' => 'Home',
                ),
                'activeGuild' => $this->getActiveGuild($session)
            )
        );
    }

    /**
     * @param Session $session
     * @return array|mixed
     */
    public function getActiveGuild(Session $session)
    {
        $activeGuild = $session->get('active-guild');

        if(!is_null($activeGuild)) {
            return $activeGuild;
        }

        return array(
            'id' => '',
            'name' => 'None',
        );
    }

    /**
     * @param Guild $guild
     */
    protected function setActiveGuild(Guild $guild)
    {
        $session = $this->get('session');
        $session->set('active-guild', array('id' => $guild->id, 'name' => $guild->name ));
    }

    /**
     * @Route("/servers", name="servers")
     */
    public function authAction(Request $request)
    {
        /** @var Session $session */
        $session = $this->get("session");

        $token = $session->get('discord-token');

        $provider = new \Discord\OAuth\Discord([
            'clientId' => $this->container->getParameter('discord_client_id'),
            'clientSecret' => $this->container->getParameter('discord_client_secret'),
            'redirectUri' => $this->container->getParameter('redirect_url'),
        ]);

        $scopes = array ( 'scope' => array(
            'identify',
            'guilds')
        );

        $guilds = null;
        $user = null;
        $ownedGuilds = [];

        if (!is_null($token)) {

            // Get the user object.
            $user = $provider->getResourceOwner($token);

            // Get the guilds and connections.
            $guilds = $user->guilds;

            foreach($guilds as $guild) {

                $permissions = $guild->__get('permissions');

                if($guild->__get('owner') == $user->getId()) {
                    array_push($ownedGuilds, $guild);
                } elseif (
                (bool) (($permissions >> 5) & 1)
                ) {
                    array_push($ownedGuilds, $guild);
                }
            }

            return $this->render(
                'AppBundle:Panel:profile.html.twig',
                array(
                    'provider' => $provider,
                    'guilds' => $ownedGuilds,
                    'user' => $user,
                    'record' => array(
                        'class' => '',
                        'displayTitle' => 'Home',
                    ),
                    'activeGuild' => $this->getActiveGuild($session)
                )
            );

        } elseif( $request->get('code')) {
            $token = $provider->getAccessToken('authorization_code', [
                'code' => $request->get('code'),
            ]);

            // Get the user object.
            $user = $provider->getResourceOwner($token);

            // Get the guilds and connections.

            $guilds = $user->guilds;

            // Store the new token.
            $session->set('discord-token', $token);
            //  $session->set('discord-guilds', $guilds);
            //  $session->set('discord-user', $user);

            foreach($guilds as $guild) {

                $permissions = $guild->__get('permissions');

                if($guild->__get('owner') == $user->getId()) {
                    array_push($ownedGuilds, $guild);
                } elseif (
                (bool) (($permissions >> 5) & 1)
                ) {
                    array_push($ownedGuilds, $guild);
                }
            }

            return $this->render(
                'AppBundle:Panel:profile.html.twig',
                array(
                    'provider' => $provider,
                    'guilds' => $ownedGuilds,
                    'user' => $user,
                    'record' => array(
                        'class' => '',
                        'displayTitle' => 'Servers',
                    ),
                    'activeGuild' => $this->getActiveGuild($session)
                )
            );
        }

        return $this->redirect($provider->getAuthorizationUrl( $scopes ));
    }

    /**
     * @param $token
     * @return array
     */
    public function getDiscordInfo($token) {

        if (is_null($token)) {
            return array(
                'user' => null,
                'guilds' => null,
            );
        }

        $provider = new \Discord\OAuth\Discord([
            'clientId' => $this->container->getParameter('discord_client_id'),
            'clientSecret' => $this->container->getParameter('discord_client_secret'),
            'redirectUri' => $this->container->getParameter('redirect_url'),
            'scope' => 'identify guilds'
        ]);

        // Get the user object.
        $user = $provider->getResourceOwner($token);

        // Get the guilds and connections.
        $guilds = $user->guilds;

        return array(
            'user' => $user,
            'guilds' => $guilds,
        );
    }

    /**
     * @Route("/guild/{guildId}", name="discord-servers")
     */
    public function discordServerAction($guildId, Request $request)
    {
        /** @var Session $session */
        $session = $this->get("session");

        $token = $session->get('discord-token');

        if (!is_null($token)) {

            $info = $this->getDiscordInfo($token);

            $ownsGuild = false;
            $guild = null;

            foreach($info['guilds'] as $loopguild) {

                if($loopguild->__get('id') == $guildId) {
                    $ownsGuild = true;
                    $guild = $loopguild;
                }
            }

            if($ownsGuild) {

                $this->setActiveGuild($guild);

                $discordServer = $this->getGuild($guildId);

                $isSaved = $this->saveGuild($discordServer, $request->request->all());

                return $this->render(
                    'AppBundle:Panel:guild.html.twig',
                    array(
                        'guild' => $guild,
                        'user' => $info['user'],
                        'server' => $discordServer,
                        'saved' => $isSaved,
                        'activeGuild' =>  $this->getActiveGuild($session),
                        'record' => array(
                            'class' => '',
                            'displayTitle' => $guild->name,
                        ),
                    )
                );
            } else {
                return $this->redirectToRoute('servers');
            }
        }

        return $this->redirectToRoute('servers');
    }

    public function getEntityManager() {
        return $this->getDoctrine()->getEntityManager();
    }

    /**
     * Gets the Guild from DB.
     * @param string $id
     * @return DiscordServer|null
     */
    public function getGuild($guildId)
    {
        /** @var DiscordServerRepository $discordServerRepo */
        $discordServerRepo = $this->getEntityManager()->getRepository("AppBundle:DiscordServer");

        /** @var DiscordServer|null $discordServer */
        $discordServer = $discordServerRepo->findOneBy(array('guildId' => $guildId));

        if(!is_null($discordServer)) {
            return $discordServer;
        }

        $newDiscordGuild = new DiscordServer();
        $newDiscordGuild->setGuildId($guildId);

        return $newDiscordGuild;
    }


    /**
     * Maps the form submission on the correct fields ( Must be made into symfony forms one day
     * Temp solution
     * @param DiscordServer $discordServer
     * @param array $postData
     * @return boolean
     */
    public function saveGuild($discordServer, $postData)
    {
        if(is_null($postData) || count($postData) < 1 )
        {
            return false;
        }

        /** @var BattlEyeRcon $bercon */
        $bercon = $discordServer->getBercon();

        $oldActions = $bercon->getSharedActions();
        $bercon->setSharedActions(array());

        foreach ($oldActions as $key => $action)
        {
            $this->removeEntity($action);
        }

        $sharedActions = array();

        $enabledPassed = false;

        foreach ($postData as $key => $value)
        {
            if($key == 'enabled') {
                $enabledPassed = true;
                $bercon->setEnabled(true);
            }
            if($key == 'colors') {
                $bercon->setColors($value);
            }

            if(strpos($key, 'command') !== false) {
                $index = substr($key, 7);

                /** @var Action $action */
                $action = Action::create($value, $postData['reply' . $index], $postData['discordReply' . $index], $postData['role' . $index] );
                array_push($sharedActions, $action);
            }

            if($key == 'permissions') {
                $bercon->setPermissions($value);
            }
        }

        if(!$enabledPassed) {
            $bercon->setEnabled(false);
        }

        if(count($sharedActions) > 0) {
            $bercon->setSharedActions($sharedActions);
        }

        $this->saveEntity($discordServer);
        return true;
    }

    /**
     * Saves something to database
     * @param * $object
     */
    public function saveEntity($object)
    {
        $this->getEntityManager()->persist($object);
        $this->getEntityManager()->flush();
    }

    /**
     * Saves something to database
     * @param * $object
     */
    public function removeEntity($object)
    {
        $this->getEntityManager()->persist($object);
    }

    /**
     * @Route("/guild/{guildId}/bercon/{berconId}", name="bercon_form")
     */
    public function berconAction($guildId, $berconId, Request $request)
    {
        /** @var Session $session */
        $session = $this->get("session");

        $token = $session->get('discord-token');

        if (!is_null($token)) {

            $info = $this->getDiscordInfo($token);

            $ownsGuild = false;
            $guild = null;

            foreach($info['guilds'] as $loopguild) {
                if($loopguild->__get('id') == $guildId) {
                    $ownsGuild = true;
                    $guild = $loopguild;
                }
            }

            if($ownsGuild) {
                /** @var DiscordServer $discordServer */
                $discordServer = $this->getGuild($guildId);
                $beRconServer = null;
                $new = true;

                if($berconId != 'new') {
                    foreach ($discordServer->getBercon()->getServers() as $server) {
                        /** @var BeRconServer $server */
                        if ($berconId == $server->getId()) {
                            $beRconServer = $server;
                            $new = false;
                        }
                    }
                } else {
                    $beRconServer = new BeRconServer();

                }

                if (is_null($beRconServer)) {
                    return $this->redirectToRoute('discord-servers', array('guildId' => $guildId));
                }

                $this->saveBerconForm($discordServer, $beRconServer, $request->request->all());

                return $this->render(
                    'AppBundle:Panel:bercon.html.twig',
                    array(
                        'guild' => $guild,
                        'user' => $info['user'],
                        'server' => $discordServer,
                        'bercon' => $beRconServer,
                        'saved' => !$new,
                        'record' => array(
                            'class' => '',
                            'displayTitle' => 'Home',
                        ),
                        'activeGuild' => $this->getActiveGuild($session)
                    )
                );
            } else {
                return $this->redirectToRoute('servers');
            }
        }

        return $this->redirectToRoute('servers');
    }

    /**
     * Maps the bercon form submission on the correct fields ( Must be made into symfony forms one day

     * @param DiscordServer $discordServer
     * @param BeRconServer $beRconServer
     * @param array $postData
     */
    public function saveBerconForm($discordServer, $beRconServer, $postData)
    {
        if(is_null($postData) || count($postData) < 1 )
        {
            return false;
        }

        $actions = array();
        $jobs = array();
        $bercon = $discordServer->getBercon();

        $beRconServer->setBercon($bercon);

        foreach ($postData as $key => $value)
        {
            if($key == 'name') {
                $beRconServer->setName($value);
            }
            if($key == 'ip') {
                $beRconServer->setIp($value);
            }
            if($key == 'port') {
                $beRconServer->setPort($value);
            }
            if($key == 'rconPassword') {
                $beRconServer->setRconPassword($value);
            }
            if($key == 'channels') {
                $beRconServer->setChannels($value);
            }
            if($key == 'timezone') {
                $beRconServer->setTimezone($value);
            }
            if($key == 'showChannels') {
                $defaultValues = $beRconServer->getShowChannels();
                $newValues = array();
                foreach ($defaultValues as $showKey => $showValue) {
                    if(array_key_exists($showKey, $value) && $value[$showKey] == 'on') {
                        $newValues[$showKey] = true;
                    } else {
                        $newValues[$showKey] = false;
                    }
                }

                $newValues['default'] = true;
                $newValues['commands'] = true;

                $beRconServer->setShowChannels($newValues);
            }
            if(strpos($key, 'command') !== false) {
                $index = substr($key, 7);
                $action = Action::create($value, $postData['reply' . $index], $postData['discordReply' . $index], $postData['role' . $index] );
                array_push($actions, $action);
            }
            if(strpos($key, 'jobtime') !== false) {
                $index = substr($key, 7);
                $job = Job::create(
                    $postData['jobtime' . $index],
                    $postData['jobtext' . $index]
                );

                array_push($jobs, $job);
            }
        }

        foreach ($beRconServer->getJobs()  as $key => $job)
        {
            $this->removeEntity($job);
        }

        foreach ($beRconServer->getActions() as $key => $action)
        {
            $this->removeEntity($action);
        }

        $beRconServer->setActions($actions);
        $beRconServer->setJobs($jobs);

        $id = $beRconServer->getId();
        if(is_null($id)) {
            $servers = $bercon->getServers()->toArray();
            array_push($servers, $beRconServer);
            $discordServer->getBercon()->setServers($servers);
        }

        $this->saveEntity($discordServer);

    }

    /**
     * Maps the bercon form submission on the correct fields ( Must be made into symfony forms one day
     * Temp solution
     * @param Server $rconserver
     * @param BerconServer $berconserver
     * @param array $postData
     * @return array
     */
    public function mapBerconForm($rconserver, $berconserver, $postData, $isNew)
    {
        $saved = false;
        if(!is_null($postData) && count($postData) > 0 )
        {
            $actions = array();
            $jobs = array();

            $bercon = $rconserver->getBercon();

            $newBerconServerId = count($bercon->getServers());

            foreach ($postData as $key => $value)
            {
                if($key == 'name') {
                    $berconserver->setName($value);
                }
                if($key == 'ip') {
                    $berconserver->setIp($value);
                }
                if($key == 'port') {
                    $berconserver->setPort($value);
                }
                if($key == 'rconPassword') {
                    $berconserver->setRconPassword($value);
                }
                if($key == 'channels') {
                    $berconserver->setChannels($value);
                }
                if($key == 'timezone') {
                    $berconserver->setTimezone($value);
                }
                if($key == 'showChannels') {
                    $defaultValues = $berconserver->getShowChannels();
                    $newValues = array();
                    foreach ($defaultValues as $showKey => $showValue) {
                        if(array_key_exists($showKey, $value) && $value[$showKey] == 'on') {
                            $newValues[$showKey] = true;
                        } else {
                            $newValues[$showKey] = false;
                        }
                    }

                    $newValues['default'] = true;
                    $newValues['commands'] = true;

                    $berconserver->setShowChannels($newValues);
                }
                if(strpos($key, 'command') !== false) {
                    $index = substr($key, 7);

                    $action = new Action($value, $postData['reply' . $index], $postData['discordReply' . $index], $postData['role' . $index] );
                    array_push($actions, $action);
                }
                if(strpos($key, 'jobtime') !== false) {
                    $index = substr($key, 7);

                    $job = new Job(
                        $postData['jobtime' . $index],
                        $postData['jobtext' . $index]
                    );

                    array_push($jobs, $job);
                }
            }

            if(count($actions) > 0) {
                $berconserver->setActions($actions);
            }

            if(count($jobs) > 0) {
                $berconserver->setJobs($jobs);
            }

            if($isNew) {
                $berconserver->setId($newBerconServerId);
                $servers = $bercon->getServers();
                array_push($servers, $berconserver);
                $rconserver->getBercon()->setServers($servers);
            }

            $this->saveGuild($rconserver);

            $saved = true;

        } else {
            if($isNew) {
                $berconserver->setId(count($rconserver->getBercon()->getServers()));
            }
        }

        return array(
            'server' => $rconserver,
            'berconServer' => $berconserver,
            'saved' => $saved,
        );
    }

    /**
     * @Route("/guild/{guildId}/bercon/{berconId}/delete", name="bercon_form_delete")
     */
    public function berconDeleteAction($guildId, $berconId)
    {
        /** @var Session $session */
        $session = $this->get("session");

        $token = $session->get('discord-token');

        if (!is_null($token)) {

            $info = $this->getDiscordInfo($token);

            $ownsGuild = false;
            $guild = null;

            foreach($info['guilds'] as $loopguild) {
                if($loopguild->__get('id') == $guildId) {
                    $ownsGuild = true;
                }
            }

            if($ownsGuild) {
                foreach ($this->getGuild($guildId)->getBercon()->getServers() as $key => $server) {
                    /** @var \AppBundle\Entity\BeRconServer $server */
                    if($berconId == $server->getId()) {
                        $server->setDeleted(true);
                        $server->setBercon(null);
                        $this->saveEntity($server);
                    }
                }

                return $this->redirectToRoute('discord-servers', array('guildId' => $guildId));

            } else {
                return $this->redirectToRoute('servers');
            }
        }
        return $this->redirectToRoute('servers');
    }

    /**
     * Gets the symfony cache.
     * @return object
     */
    public function getCache() {
        return $this->get('cache.app');
    }

}

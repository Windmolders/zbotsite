<?php

namespace AppBundle\Controller;

use AppBundle\Entity\SiteSetting;
use Clastic\BackofficeBundle\Form\Type\LinkType;
use Clastic\BackofficeBundle\Form\Type\TabsTabActionsType;
use Clastic\BackofficeBundle\Form\Type\TabsTabType;
use Clastic\BackofficeBundle\Form\Type\TabsType;
use Clastic\BackofficeBundle\Form\Type\WysiwygType;
use Clastic\MediaBundle\Form\Type\FileBrowserType;
use Clastic\MediaBundle\Form\Type\FileFormType;
use Clastic\NodeBundle\Module\NodeModuleInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Clastic\BlockBundle;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

class SiteSettingController extends HelperController
{

    public function indexAction(Request $request)
    {
        $this->buildBreadcrumbs('site_setting','clastic_node_list');

        /** @var \Symfony\Component\Form\Form $form */
        $form = $this->createSettingForm();

        $form->handleRequest($request);

        if ($form->isValid()) {

            $settings = $form->getData();

            foreach($settings as $key => $value) {
                $this->setSetting($key, $value);
            }

            return $this->redirectToRoute('clastic_backoffice_dashboard');
         }

        return $this->render('AppBundle:SiteSetting:edit.html.twig', array_merge ( array(
                'record' => array(
                    'icon' => 'fa-envelope-o',
                    'class' => 'blauw',
                    'displayTitle' => 'Contact',
                ),
                'form' => $form->createView(),
            ), $this->getTemplateData()
        ));
    }

    public function getSetting($name, $default = '') {
        $sitesettings = $this->getSettingRepo();

        /** @var SiteSetting $value */
        $setting = $sitesettings->findOneBy(array('name' => $name));

        if (is_null($setting)) {
            return $default;
        }

        return $setting->getValue();
    }

    public function setSetting($name, $value) {
        $em = $this->getDoctrine()->getManager();
        $sitesettings = $this->getSettingRepo();

        /** @var SiteSetting $value */
        $setting = $sitesettings->findOneBy(array('name' => $name));

        if (is_null($setting)) {
            $setting = new SiteSetting();
            $setting->setName($name);
            $setting->setValue($value);
        } else {
            $setting->setValue($value);
        }

        $em->persist($setting);
        $em->flush();
    }

    /**
     *
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    public function getSettingRepo() {
        return $this->getDoctrine()->getRepository('AppBundle:SiteSetting');
    }

    /**
     * @return \Symfony\Component\Form\Form
     */
    public function createSettingForm() {

        $defaultData = array();

        /** @var FormBuilderInterface $form */
        $builder = $this->createFormBuilder($defaultData);

        $builder
            ->add(
                $builder->create('tabs', TabsType::class, array('inherit_data' => true))
                    ->add($this->createGeneralTab($builder))
                    ->add($this->createLatestTab($builder))
                    ->add($this->createMailTab($builder))
                    ->add($this->createAboutTab($builder))
                    ->add($this->createActionTab($builder))
            );

        $this->setData($builder);

        return $builder->getForm();

    }

    /**
     * Sets all the settings on form load
     *
     * @param FormBuilderInterface $builder
     */
    private function setData(FormBuilderInterface $builder) {

        // General tab
        $tab = $builder->get('tabs')->get('general');
        $tab->get('homepageBanner')->setData($this->getSetting('homepageBanner'));
        $tab->get('homepagePosition')->setData($this->getSetting('homepagePosition'));
        $tab->get('footerBanner')->setData($this->getSetting('footerBanner'));
        $tab->get('contactPhoto')->setData($this->getSetting('contactPhoto'));
        $tab->get('siteTitle')->setData($this->getSetting('siteTitle'));
        $tab->get('siteTitleExtra')->setData($this->getSetting('siteTitleExtra'));
        $tab->get('siteSlogan')->setData($this->getSetting('siteSlogan'));
        $tab->get('siteMail')->setData($this->getSetting('siteMail'));
        $tab->get('siteInstagram')->setData($this->getSetting('siteInstagram'));
        $tab->get('policy')->setData($this->getSetting('policy'));

        // General tab
        $tab = $builder->get('tabs')->get('latestTab');
        $tab->get('latest1')->setData($this->getSetting('latest1'));
        $tab->get('latest1text')->setData($this->getSetting('latest1text'));
        $tab->get('latest2')->setData($this->getSetting('latest2'));
        $tab->get('latest2text')->setData($this->getSetting('latest2text'));
        $tab->get('latest3')->setData($this->getSetting('latest3'));
        $tab->get('latest3text')->setData($this->getSetting('latest3text'));


        // contact tab
        $tab = $builder->get('tabs')->get('mailTab');
        $tab->get('contactIntro')->setData($this->getSetting('contactIntro'));
        $tab->get('mailOnLogin')->setData($this->getSetting('mailOnLogin'));
        $tab->get('mailOnTokenCreation')->setData($this->getSetting('mailOnTokenCreation'));
        $tab->get('mailSender')->setData($this->getSetting('mailSender'));
        $tab->get('mailSenderName')->setData($this->getSetting('mailSenderName'));
        $tab->get('mailTitle')->setData($this->getSetting('mailTitle'));
        $tab->get('mailLogo')->setData($this->getSetting('mailLogo'));
        $tab->get('mailTextTitle')->setData($this->getSetting('mailTextTitle'));
        $tab->get('mailTextIntro')->setData($this->getSetting('mailTextIntro'));
        $tab->get('mailTextIntroBackend')->setData($this->getSetting('mailTextIntroBackend'));
        $tab->get('successMessage')->setData($this->getSetting('successMessage'));
        $tab->get('mailTo')->setData($this->getSetting('mailTo'));

        // General tab
        $tab = $builder->get('tabs')->get('aboutTab');
        $tab->get('aboutContactText')->setData($this->getSetting('aboutContactText'));
        $tab->get('aboutHair')->setData($this->getSetting('aboutHair'));
        $tab->get('aboutWaste')->setData($this->getSetting('aboutWaste'));
        $tab->get('aboutLength')->setData($this->getSetting('aboutLength'));
        $tab->get('aboutEyes')->setData($this->getSetting('aboutEyes'));
        $tab->get('aboutCup')->setData($this->getSetting('aboutCup'));
        $tab->get('aboutWaist')->setData($this->getSetting('aboutWaist'));
        $tab->get('aboutShoe')->setData($this->getSetting('aboutShoe'));

    }

    /**
     * @param FormBuilderInterface $builder
     *
     * @return FormBuilderInterface
     */
    private function createGeneralTab(FormBuilderInterface $builder)
    {
        return $this->createTab($builder, 'general', array('label' => 'Algemeen'))
            ->add('siteTitle', TextType::class, array(
                'label' => 'Jouw naam ( MiMich )',
                'required' => false,
            ))
            ->add('siteTitleExtra', TextType::class, array(
                'label' => 'Jouw extra naam ( PhotoModel )',
                'required' => false,
            ))
            ->add('siteSlogan', TextType::class, array(
                'label' => 'Bijschrift',
                'required' => false,
            ))
            ->add('siteMail', EmailType::class, array(
                'label' => 'Contact e-mail in footer en contact pagina',
                'required' => false,
            ))
            ->add('siteInstagram', UrlType::class, array(
                'label' => 'Instagram link',
                'required' => false,
            ))
            ->add('homepageBanner', FileBrowserType::class, array(
                'label' => 'Banner op de homepage',
                'required' => false,
            ))
            ->add('homepagePosition', ChoiceType::class, [
                'label' => 'Positie van de homepage banner',
                'expanded' => false,
                'multiple' => false,
                'choices' => [ 'Vanboven' => 'top', 'Midden' => 'center' , 'Vanonder' => 'bottom'],
                'attr' => array('class' => ''),
                'required' => true,
            ])
            ->add('footerBanner', FileBrowserType::class, array(
                'label' => 'Banner in de footer',
                'required' => false,
            ))
            ->add('contactPhoto', FileBrowserType::class, array(
                'label' => 'Foto op de contact pagina',
                'required' => false,
            ))
            ->add('policy', WysiwygType::class, array(
                'label' => 'Policy pagina tekst',
                'required' => false,
             ));

    }

    /**
     * @param FormBuilderInterface $builder
     *
     * @return FormBuilderInterface
     */
    private function createLatestTab(FormBuilderInterface $builder)
    {
        return $this->createTab($builder, 'latestTab', array('label' => 'Latest'))
            ->add('latest1', FileBrowserType::class, array(
                'label' => 'Foto 1',
                'required' => false,
            ))
            ->add('latest1text', TextareaType::class, array(
                'label' => 'Tekst voor Foto1 (Kan leeg zijn)',
                'required' => false,
            ))

            ->add('latest2', FileBrowserType::class, array(
                'label' => 'Foto 2',
                'required' => false,
            ))
            ->add('latest2text', TextareaType::class, array(
                'label' => 'Tekst voor Foto 2 (Kan leeg zijn)',
                'required' => false,
            ))

            ->add('latest3', FileBrowserType::class, array(
                'label' => 'Foto 3',
                'required' => false,
            ))
            ->add('latest3text', TextareaType::class, array(
                'label' => 'Tekst voor Foto 3 (Kan leeg zijn)',
                'required' => false,
            ))
            ;
    }

    /**
     * @param FormBuilderInterface $builder
     *
     * @return FormBuilderInterface
     */
    private function createMailTab(FormBuilderInterface $builder)
    {
        return $this->createTab($builder, 'mailTab', array('label' => 'Mailing & Contact'))
            ->add('mailOnLogin', ChoiceType::class, [
                'label' => 'Stuur een automatische mail als iemand inlogd met een token.',
                'expanded' => true,
                'multiple' => false,
                'choices' => [ 'Nee' => false, 'Ja' => true],
                'attr' => array('class' => 'radio-true-false'),
                'required' => true,
            ])
            ->add('mailOnTokenCreation', ChoiceType::class, [
                'label' => 'Stuur een automatische mail naar een persoon als je een token voor hem aangemaakt hebt.',
                'expanded' => true,
                'multiple' => false,
                'choices' => [ 'Nee' => false, 'Ja' => true],
                'attr' => array('class' => 'radio-true-false'),
                'required' => true,
            ])
            ->add('contactIntro', TextareaType::class, array(
                'label' => 'Tekst boven het contact formulier',
                'required' => false,
            ))
            ->add('successMessage', TextareaType::class, array(
                'label' => 'Info op de site wanneer contact ingevuld en verzonden is',
                'required' => false,
            ))
            ->add('mailSender', TextType::class, array(
                'label' => 'E-mailadres van zender (contact@mimich.be)',
                'required' => false,
            ))
            ->add('mailSenderName', TextType::class, array(
                'label' => 'Alias/Naam van zender (MiMich)',
                'required' => false,
            ))
            ->add('mailTitle', TextType::class, array(
                'label' => 'Titel van de mail',
                'required' => false,
            ))
            ->add('mailLogo', FileBrowserType::class, array(
                'label' => 'Logo bovenaan de mail',
                'required' => false,
            ))
            ->add('mailTextTitle', TextType::class, array(
                'label' => 'Titel in de mail',
                'required' => false,
            ))
            ->add('mailTextIntro', TextareaType::class, array(
                'label' => 'Intro tekst na de aanspreking',
                'required' => false,
            ))
            ->add('mailTextIntroBackend', TextareaType::class, array(
                'label' => 'Intro tekst naar jouw',
                'required' => false,
            ))
            ->add('mailTo', TextareaType::class, array(
                'label' => 'Verzenden naar jouw email-adressen ( vb: contact@mimich.be, model.mimich@gmail.com)',
                'required' => false,
            ))
            ;
    }

    /**
     * @param FormBuilderInterface $builder
     *
     * @return FormBuilderInterface
     */
    private function createAboutTab(FormBuilderInterface $builder)
    {
        return $this->createTab($builder, 'aboutTab', array('label' => 'About me'))
            ->add('aboutContactText', TextType::class, array(
                'label' => 'Contact tekst boven knop',
                'required' => false,
            ))
            ->add('aboutHair', TextType::class, array(
                'label' => 'Haar',
                'required' => false,
            ))
            ->add('aboutWaste', TextType::class, array(
                'label' => 'Maat',
                'required' => false,
            ))
            ->add('aboutLength', TextType::class, array(
                'label' => 'Lengte',
                'required' => false,
            ))
            ->add('aboutEyes', TextType::class, array(
                'label' => 'Ogen',
                'required' => false,
            ))
            ->add('aboutCup', TextType::class, array(
                'label' => 'Cup',
                'required' => false,
            ))
            ->add('aboutWaist', TextType::class, array(
                'label' => 'BBB',
                'required' => false,
            ))
            ->add('aboutShoe', TextType::class, array(
                'label' => 'Schoenmaat',
                'required' => false,
            ))

            ;
    }


    /**
     * @param FormBuilderInterface $builder
     * @param string               $name
     *
     * @deprecated Use TabHelper instead.
     *
     * @return FormBuilderInterface
     */
    protected function findTab(FormBuilderInterface $builder, $name)
    {
        return $builder->get('tabs')->get($name);
    }

    private function createActionTab(FormBuilderInterface $builder)
    {
        return $builder->create('actions', TabsTabActionsType::class, array(
            'mapped' => false,
            'inherit_data' => true,
        ))
            ->add('save', SubmitType::class, array(
                'label' => 'node.form.tab.action.field.save',
                'attr' => array('class' => 'btn btn-success'),
            ));
    }

    /**
     * @param FormBuilderInterface $builder
     * @param string               $name
     * @param array                $options
     *
     * @return FormBuilderInterface
     */
    private function createTab(FormBuilderInterface $builder, $name, $options = array())
    {
        $options = array_replace(
            $options,
            array(
                'inherit_data' => true,
            ));

        return $builder->create($name, TabsTabType::class, $options);
    }

    /**
     * @return string
     */
    public function getType() {
        return 'site_setting';
    }

    /**
     * @return string
     */
    public function getEntityType() {
        return 'site_setting';
    }

    /**
     * @return string
     */
    public function getEntityName() {
        return 'AppBundle:SiteSetting';
    }

    /**
     * @return string
     */
    public function getDisplayTitle() {
        return 'Instellingen';
    }

    /**
     * Returns array with extra template data
     *
     * @return array
     */
    public function getTemplateData() {
        return array(
            'type' => $this->getType(),
            'entityType' => $this->getEntityType(),
            'module' => $this->get('clastic.module_manager')->getModule($this->getType()),
            'submodules' => array(),
            'links' => array(),
            'displayTitle' => $this->getDisplayTitle(),
        );
    }

}

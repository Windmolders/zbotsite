<?php

namespace AppBundle\Twig;

use AppBundle\Repository\SiteSettingRepository;
use AppBundle\Service\ConfigInstance;
use Clastic\AliasBundle\Entity\Alias;
use Clastic\AliasBundle\Entity\AliasRepository;
use Clastic\MenuBundle\Entity\MenuItem;
use Clastic\MenuBundle\Entity\MenuItemRepository;
use Clastic\NodeBundle\Entity\Node;
use Symfony\Bundle\FrameworkBundle\Templating\GlobalVariables;
use Symfony\Component\Finder\Finder;
use Doctrine\ORM\Query\Expr\Join;

class TwigExtension extends \Twig_Extension
{
    /**
     * @var \Twig_Environment
     */
    private $environment;

    /**
     * @var MenuItemRepository
     */
    private $repoMenuItem;

    /**
     * @var AliasRepository
     */
    private $repoAlias;

    /**
     * @var ConfigInstance
     */
    private $config;

    /**
     * @return ConfigInstance
     */
    protected function getConfig()
    {
        return $this->config;
    }

    /**
     * @param ConfigInstance $config
     */
    protected function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return \Twig_Environment
     */
    public function getEnvironment()
    {
        return $this->environment;
    }

    /**
     * @param \Twig_Environment $environment
     */
    public function setEnvironment($environment)
    {
        $this->environment = $environment;
    }

    /**
     * @var SiteSettingRepository
     */
    private $repoSiteSetting;

    /**
     * @param MenuItemRepository $repository
     */
    public function __construct(MenuItemRepository $repository, SiteSettingRepository $repoSiteSetting, AliasRepository $repoAlias, ConfigInstance $configInstance)
    {
        $this->repoMenuItem = $repository;
        $this->repoSiteSetting = $repoSiteSetting;
        $this->repoAlias = $repoAlias;
        $this->config = $configInstance;
    }

    /**
     * {@inheritdoc}
     */
    public function initRuntime(\Twig_Environment $environment)
    {
        $this->environment = $environment;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('site_clastic_menu', array($this, 'renderMenu'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('site_setting', array($this, 'renderSetting'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('site_img_setting', array($this, 'renderSettingImg'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('site_folder', array($this, 'searchFolder')),
            new \Twig_SimpleFunction('site_alias', array($this, 'getAlias')),
            new \Twig_SimpleFunction('site_alias_node', array($this, 'getAliasByNode')),
            new \Twig_SimpleFunction('site_tofront', array($this, 'toFront')),

        );
    }

    /**
     * @return string
     */
    public function getAlias($path = '')
    {
        /** @var Alias $alias */
        $alias = $this->repoAlias->findOneBy(array('path' => $path));

        if (is_null($alias)) {
            return '/';
        }

        return '/' . $alias->getAlias();
    }

    /**
     * @return string
     */
    public function getAliasByNode(Node $node)
    {
        /** @var Alias $alias */
        $alias = $this->repoAlias->findOneBy(array('node_id' => $node->getId()));

        if (is_null($alias)) {
            return '/';
        }

        return '/' . $alias->getPath();
    }

    /**
     * @return string
     */
    public function toFront($path)
    {
        $string = str_replace('\\', '/', $path);

        return $string;
    }

    /**
     * @return array
     */
    public function searchFolder($folder = '')
    {
        $finder = new Finder();

        $finder
            ->files()
            ->in($folder);

        return iterator_to_array($finder);
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('sortByProp', array($this, 'sortByProperty')),
            new \Twig_SimpleFilter('sortByWeight', array($this, 'sortByWeight')),
        );
    }

    /**
     * @param array $array
     * @param string $property
     *
     * @return array
     */
    public function sortByProperty($array)
    {
        uasort($array, array($this, "cmp"));

        return $array;
    }

    function cmp($a, $b)
    {
        return strcmp($a->getName(), $b->getName());
    }

    /**
     * @param array $array
     * @param string $property
     *
     * @return array
     */
    public function sortByWeight($array)
    {
        $var = (array)$array;

        uasort($var, array($this, "cmpWeight"));

        return $var;
    }

    function cmpWeight($a, $b)
    {
        return strcmp($a->getWeight(), $b->getWeight());
    }



    /**
     * @param string $settingName
     * @param string $default
     *
     * @return string
     */
    public function renderSetting($settingName, $default = '')
    {
        return $this->getConfig()->getConfigValue($settingName, $default);
    }

    /**
     * @param string $settingName
     * @param string $default
     *
     * @return string
     */
    public function renderSettingImg($settingName, $default = '')
    {
        return substr($this->getConfig()->getConfigValue($settingName, $default), 1);
    }

    /**
     * @param string            $menuIdentifier
     * @param string            $type
     * @param int               $depth
     *
     * @return string
     */
    public function renderMenu($menuIdentifier, $depth = 3, $type = 'main')
    {
        $queryBuilder = $this->repoMenuItem->getNodesHierarchyQueryBuilder(null, false, array(), true)
            ->join('ClasticMenuBundle:Menu', 'menu', Join::WITH, 'menu.id = node.menu')
            ->andWhere('menu.identifier = :identifier')
            ->setParameter('identifier', $menuIdentifier);

        $globals = $this->environment->getGlobals();

        /** @var GlobalVariables $variables */
        $variables = $globals['app'];
        $currentUrl = $variables->getRequest()->server->get('REQUEST_URI');

        $items = array_map(function (MenuItem $item) use ($currentUrl) {

            $url = $item->getUrl();
            $node = $item->getNode();
            // TODO Remove getTitle once a solution is found.
            if ($node && $node->getTitle() && isset($node->alias)) {
                $url = '/'.$node->alias->getAlias();
            }

            return array(
                'title' => $item->getTitle(),
                'left' => $item->getLeft(),
                'right' => $item->getRight(),
                'root' => $item->getRoot(),
                'level' => $item->getLevel(),
                'id' => $item->getId(),
                '_node' => $item,
                '_active' => ($url == $currentUrl),
                '_link' => $url,
            );
        }, $queryBuilder->getQuery()->getResult());

        $template = 'AppBundle:Twig:menu.html.twig';
        if( $type == 'footer') {
            $template = 'AppBundle:Twig:menu_footer.html.twig';
        }
        if( $type == 'footer-left') {
            $template = 'AppBundle:Twig:menu_footer_links.html.twig';
        }
        if( $type == 'side') {
            $template = 'AppBundle:Twig:side.html.twig';
        }

        return $this->environment->render($template, array(
            'tree' => $this->repoMenuItem->buildTree($items),
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'twig';
    }
}

<?php

namespace AppBundle\Object;

class Action
{
    function __construct($command, $reply, $discordReply, $role)
    {
        $this->command = $command;
        $this->reply = $reply;
        $this->discordReply = $discordReply;
        $this->role = $role;
    }

    /**
     * @var string $command
     */
    private $command;

    /**
     * @var string $reply
     */
    private $reply;

    /**
     * @var string $discordReply;
     */
    private $discordReply;

    /**
     * @var string $role;
     */
    private $role;

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param string $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * @return string
     */
    public function getCommand()
    {
        return $this->command;
    }

    /**
     * @param string $command
     */
    public function setCommand($command)
    {
        $this->command = $command;
    }

    /**
     * @return string
     */
    public function getReply()
    {
        return $this->reply;
    }

    /**
     * @param string $reply
     */
    public function setReply($reply)
    {
        $this->reply = $reply;
    }

    /**
     * @return string
     */
    public function getDiscordReply()
    {
        return $this->discordReply;
    }

    /**
     * @param string $discordReply
     */
    public function setDiscordReply($discordReply)
    {
        $this->discordReply = $discordReply;
    }


}

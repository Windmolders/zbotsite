<?php

namespace AppBundle\Object;

class Server
{
    function __construct()
    {
        $this->bercon = new Bercon();
    }

    /**
     * @var $bercon Bercon
     */
    private $bercon;

    /**
     * @var $guildId string
     */
    private $guildId;

    /**
     * @var $channels array
     */
    private $channels = array('welcome' => '', 'goodbye' => '' );

    /**
     * @return Bercon
     */
    public function getBercon()
    {
        return $this->bercon;
    }

    /**
     * @param Bercon $bercon
     */
    public function setBercon($bercon)
    {
        $this->bercon = $bercon;
    }

    /**
     * @return string
     */
    public function getGuildId()
    {
        return $this->guildId;
    }

    /**
     * @param string $guildId
     */
    public function setGuildId($guildId)
    {
        $this->guildId = $guildId;
    }

    /**
     * @return array
     */
    public function getChannels()
    {
        return $this->channels;
    }

    /**
     * @param array $channels
     */
    public function setChannels($channels)
    {
        $this->channels = $channels;
    }

}

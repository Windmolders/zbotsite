<?php

namespace AppBundle\Object;

class Job
{
    function __construct($time, $text)
    {
        $this->time = $time;
        $this->text = $text;
    }

    /**
     * @var string $time
     */
    private $time;

    /**
     * @var string $text
     */
    private $text;

    /**
     * @return string
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param string $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }





}

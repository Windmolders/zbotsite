<?php

namespace AppBundle\Object;

class BerconServer
{
    function __construct()
    {
        $this->actions = array(new Action('','','',''));
        $this->jobs = array(new Job('',''));
    }

    private $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @var string $encryptionKey;
     */
    private $encryptionKey = '4zcMONp61gpVDcuckG0u';

    /**
     * @return string
     */
    public function getEncryptionKey()
    {
        return $this->encryptionKey;
    }

    /**
     * @var int $timezone
     */
    private $timezone = 2;

    /**
     * @return int
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * @param int $timezone
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;
    }

    /**
     * @var string $name;
     */
    private $name;

    /**
     * @var string $ip;
     */
    private $ip;

    /**
     * @var string $port;
     */
    private $port;

    /**
     * @var string $rconPassword;
     */
    private $rconPassword;

    /**
     * @var array<Action> $actions;
     */
    private $actions;

    /**
     * @var array<Job> $actions;
     */
    private $jobs;

    /**
     * @var array<string,string> $channels
     */
    private $channels = array (
        'side' => 'rcon',
        'direct' => 'rcon',
        'vehicle' => 'rcon',
        'group' => 'rcon',
        'admin' => 'rcon',
        'default' => 'rcon',
        'commands' => 'rcon',
        'joins' => 'rcon',
        'global' => 'rcon',
    );

    /**
     * @var array<string,bool> $channels
     */
    private $showChannels = array (
        'side' => true,
        'direct' => true,
        'vehicle' => true,
        'group' => true,
        'admin' => true,
        'default' => true,
        'commands' => true,
        'joins' => true,
        'global' => true,
    );



    /**
     * @return array
     */
    public function getJobs()
    {
        return $this->jobs;
    }

    /**
     * @param array $jobs
     */
    public function setJobs($jobs)
    {
        $this->jobs = $jobs;
    }



    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return string
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @param string $port
     */
    public function setPort($port)
    {
        $this->port = $port;
    }

    /**
     * @return string
     */
    public function getRconPassword()
    {
        return $this->rconPassword;
    }

    /**
     * @param string $rconPassword
     */
    public function setRconPassword($rconPassword)
    {
        $this->rconPassword = $rconPassword;
    }

    /**
     * @return array
     */
    public function getActions()
    {
        return $this->actions;
    }

    /**
     * @param array $actions
     */
    public function setActions($actions)
    {
        $this->actions = $actions;
    }

    /**
     * @return array
     */
    public function getChannels()
    {
        return $this->channels;
    }

    /**
     * @param array $channels
     */
    public function setChannels($channels)
    {
        $this->channels = array_merge($this->channels ,$channels);
    }

    /**
     * @return array
     */
    public function getShowChannels()
    {
        return $this->showChannels;
    }

    /**
     * @param array $showChannels
     */
    public function setShowChannels($showChannels)
    {
        $this->showChannels = array_merge($this->showChannels, $showChannels);
    }




}

<?php

namespace AppBundle\Object;

class Bercon
{
    function __construct()
    {
        $this->sharedActions = array(new Action('','','',''));
    }

    /**
     * @var bool $enabled
     */
    private $enabled = false;

    /**
     * @var bool $colors
     */
    private $colors = true;

    /**
     * @var array<BerconServers>
     */
    private $servers = array();

    /**
     * @var array<Action> $sharedActions;
     */
    private $sharedActions = array(

    );

    private $permissions = array(
        'players' =>  'rcon-admin',
        'admins' =>  'rcon-admin',
        'bans' =>  'rcon-admin',
        'loadScripts' =>  'rcon-admin',
        'loadEvents' =>  'rcon-admin',
        'say' =>  'rcon-admin',
        'missions' =>  'rcon-admin',
        'version' =>  'rcon-admin',
        'update' =>  'rcon-admin',
        'loadBans' =>  'rcon-admin',
        'writeBans' =>  'rcon-admin',
        'removeBan' =>  'rcon-admin',
        'ban' =>  'rcon-admin',
        'addBan' =>  'rcon-admin',
        'MaxPing' =>  'rcon-admin',
        'kick' =>  'rcon-admin',
        'serverCommands' =>  'rcon-admin',
        'disconnect' =>  'rcon-admin',
        'exit' =>  'rcon-admin',
    );

    /**
     * @return array
     */
    public function getSharedActions()
    {
        return $this->sharedActions;
    }

    /**
     * @param array $sharedActions
     */
    public function setSharedActions($sharedActions)
    {
        $this->sharedActions = $sharedActions;
    }

    /**
     * @return array
     */
    public function getPermissions()
    {
        return $this->permissions;
    }

    /**
     * @param array $permissions
     */
    public function setPermissions($permissions)
    {
        $this->permissions = $permissions;
    }



    /**
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param boolean $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return boolean
     */
    public function isColors()
    {
        return $this->colors;
    }

    /**
     * @param boolean $colors
     */
    public function setColors($colors)
    {
        $this->colors = $colors;
    }

    /**
     * @return array
     */
    public function getServers()
    {
        return $this->servers;
    }

    /**
     * @param array $servers
     */
    public function setServers($servers)
    {
        $this->servers = $servers;
    }

}

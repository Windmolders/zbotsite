(function($){
  $(function(){ // doc ready

    $menubutton = $('.js-button-sidebar');
    $sidebar = $('.js-sidebar');
    $main = $('.main-body');

    function toggleMenu(isOpen) {
      if(isOpen) {
        $main.removeClass('sidebar-mini');
        $sidebar.removeClass('sidebar-mini');
      } else {
        $main.addClass('sidebar-mini');
        $sidebar.addClass('sidebar-mini');
      }
    }

    $(".button-collapse").sideNav();



    $menubutton.on('click', function() {
      if($menubutton.hasClass('active')) {
        $menubutton.removeClass('active');
        toggleMenu(false)
      } else {
        $menubutton.addClass('active');
        toggleMenu(true)
      }
    });

    var navSafeHeight = 90;

    $('.scrollspy').scrollSpy();


    $('.js-back-to-top').pushpin({ top: 60 });

    $('.js-back-to-top').on('click', function(e){
      e.preventDefault();
      $("html, body").animate({ scrollTop: 0 }, "slow");
    });

    var regex = /^(.+?)(\d+)$/i;
    var cloneIndex = $(".action").length;

    function clone(e){
      e.preventDefault();

      $(this).parents(".action").clone()
        .appendTo(".actionParent")
        .attr("id", "action" +  cloneIndex)
        .find("*")
        .each(function() {
          var id = this.id || "";
          var name = this.name || "";
          var forlabel = this.for || "";

          var match = id.match(regex) || [];
          if (match.length == 3) {
            this.id = match[1] + (cloneIndex);
          }

          match = name.match(regex) || [];
          if (match.length == 3) {
            this.name = match[1] + (cloneIndex);
          }

          match = forlabel.match(regex) || [];
          if (match.length == 3) {
            this.for = match[1] + (cloneIndex);
          }

        })
        .on('click', 'a.js-clone', clone)
        .on('click', 'a.js-remove', remove);
      cloneIndex++;
    }

    function remove(e){
      e.preventDefault();
      if($(".action").length > 1) {
        $(this).parents(".action").remove();
      } else {
        $(this).parents(".action").find('input').val('');
      }
    }

    var regexJob = /^(.+?)(\d+)$/i;
    var cloneIndexJob = $(".job").length;

    function cloneJob(e){
      e.preventDefault();

      $(this).parents(".job").clone()
        .appendTo(".jobParent")
        .attr("id", "job" +  cloneIndex)
        .find("*")
        .each(function() {
          var id = this.id || "";
          var name = this.name || "";
          var forlabel = this.for || "";

          var match = id.match(regexJob) || [];
          if (match.length == 3) {
            this.id = match[1] + (cloneIndexJob);
          }

          match = name.match(regexJob) || [];
          if (match.length == 3) {
            this.name = match[1] + (cloneIndexJob);
          }

          match = forlabel.match(regexJob) || [];
          if (match.length == 3) {
            this.for = match[1] + (cloneIndexJob);
          }

        })
        .on('click', 'a.js-clone-job', cloneJob)
        .on('click', 'a.js-remove-job', removeJob);
      cloneIndexJob++;
    }



    function removeJob(e){
      e.preventDefault();
      if($(".job").length > 1) {
        $(this).parents(".job").remove();
      } else {
        $(this).parents(".job").find('input').val('');
      }
    }

    $("a.js-clone").on("click", clone);

    $("a.js-remove").on("click", remove);

    $("a.js-clone-job").on("click", cloneJob);

    $("a.js-remove-job").on("click", removeJob);


    $("a.js-delete-server").on('click', function(e) {
      if (confirm('Are you sure you want to delete this server?')) {

      } else {
        e.preventDefault();
      }
    });


    if( $('.js-saved').length > 0) {
      Materialize.toast('Page saved!', 4000); // 4000 is the duration of the toast
    }




  }); // end of document ready
})(jQuery); // end of jQuery name space
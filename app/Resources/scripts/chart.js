(function($){
  $(function(){

    var ctx = $("#pastry-eaters");

    if(ctx.length == 0) {
      return;
    }

    var labels = ctx.data('labels');
    var amounts = ctx.data('amounts');

    var data = {
      labels: labels,
      datasets: [
        {
          label: "Pastry Eaters per week",
          fill: true,
          lineTension: 0.1,
          backgroundColor: "rgba(75,192,192,1)",
          borderColor: "rgba(75,192,192,1)",
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: 'miter',
          pointBorderColor: "rgba(75,192,192,1)",
          pointBackgroundColor: "#fff",
          pointBorderWidth: 0,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: "rgba(75,192,192,1)",
          pointHoverBorderColor: "rgba(220,220,220,1)",
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          data: amounts,
          spanGaps: false,
          showLines: false
        }
      ]
    };

    var options = {
      scales: {
        xAxes: [{
          display: true
        }]
      }
    };

    var myLineChart = new Chart(ctx, {
      type: 'line',
      data: data,
      options: options
    });


  }); // end of document ready
})(jQuery); // end of jQuery name space
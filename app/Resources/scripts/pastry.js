(function($){
  $(function(){

    if(!$('body').hasClass('js-pastry')) {
      return;
    }

    var calendarInit = false;

    $('ul.tabs').tabs({
      swipeable: true,
      onShow: function(tab) {
        if(tab[0].id == 'general') {
          initCalendar(true);
        }
      }
    });

    if(window.hash == "general") {
      initCalendar(true);
    }

    var $modal = $('#modal-pastry');

    if($modal.length > 0) {
      var toastCheck = $modal.data('toast');

      if(toastCheck == 'yes') {
        Materialize.toast('You are attending!', 4000, 'teal');
      } else if (toastCheck == 'no') {
        Materialize.toast('Not attending :( ', 4000, 'red darken-3');
      }

      // $modal.modal();
      // $modal.modal('open');
    }

    function initCalendar(init) {
      if(!calendarInit) {
        console.log('start');
        $calendar = $('.calendar');
        if ($calendar.length > 0) {
          events = $calendar.data('events');
          $calendar.fullCalendar({
            weekends: true,
            events: events
          })
        }
        calendarInit = init;
      }
    }

    function createCookie(name,value,days) {
      var expires = "";
      if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
      }
      document.cookie = name + "=" + value + expires + "; path=/";
    }

    function readCookie(name) {
      var nameEQ = name + "=";
      var ca = document.cookie.split(';');
      for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
      }
      return false;
    }

    function eraseCookie(name) {
      createCookie(name,"",-1);
    }

    $('.js-hide-pastry-sign-up').on('click', function(e) {
      createCookie('hidePastrySingUp',true,256);
      $('.js-pastry-sign-up').hide();
      e.preventDefault();
    });

    if(!readCookie('hidePastrySingUp')) {
      $('.js-pastry-sign-up').show();
    }

  });
})(jQuery);